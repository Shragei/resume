'use strict';
var io=require('./socket.io.js');
var angular=require('angular');
var app=angular.module('socket',[]);

app.factory('socket',function($rootScope){
  var socket=io();
  return{
    on:function(eventName,callback){
      socket.on(eventName,function(){
        $rootScope.$apply(function(){
          callback.apply(arguments);
        });
      });
    },
    emit:function(eventName,data,callback){
      socket.emit(eventName,data,function(){
        if(callback){
          $rootScope.$apply(function(){
            callback.$apply(arguments);
          });
        }
      });
    }
  };
});