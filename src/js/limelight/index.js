var JNG=require('./JNG');
var APNG=require('./APNG');

global.LimeLightCtrl=function LimeLightCtrl($scope){
  var player;
  $scope.image=function(img){
    $scope.selected=img;
    readImage('/resources/img/'+img+".png");
  }
  function readImage(val){
    try{
      APNG(val,function(p){
        $scope.$apply(function(){
          player=p;
          $scope.limeOutput=player.Canvas();
          player.Play();
        });
      });
    }catch(e){
  /*    try{
        JNG(val,function(image){
          
        });
      }  */
    }
  }
  readImage('/resources/img/spinfox.png');
  $scope.selected="spinfox";
  
  $scope.$watch('state',function(newval){
    if(player)
      switch(newval){
        case 'play':player.Play();break;
        case 'stop':player.Pause();break;
        case 'reset':player.Reset();break;
      }
    $scope.state='clear';
  })
  $scope.$watch('limeInput',function(newval){
    if(newval instanceof ArrayBuffer)
      readImage(newval);
  });
}