# MessagePack.js
MessagePack.js is a synchronous Javascript library for handling MessagePack data streams.

### Usage

Serializing 

arraybuffer=MessagePack.Pack(Obj);

Parsing
The unpacking function will accept TypedArrays and raw ArayyBuffer objects as inputs.

var InputBuffer = new ArrayBuffer();
var OutputObj = MessagePack.Unpack(InputBuffer);


### Mappings

The minimum size for a specifed type is used when converting a Javascript obj to MessagePack binaray stream.

| Javascript  | MessagePack |
|-------------|-------------|
| Null        | Null        |
| Boolean     | Boolean     |
| ArrayBuffer | Bin         |
| Object      | Map         |
| Array       | Array       |
| Float       | Float       |
| Int         | Int         |
| Undefined   | Ext         |
| String      | String      |
| Date        | Ext         |
| RegExp      | Ext         |

### License

MessagePack.js is covered by the MIT License.