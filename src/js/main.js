'use strict';
var snap=require('snapsvg');
var $=require('jquery');
require('jquery-smooth-scroll');
var angular=require('angular');

var app=angular.module('resume',['ngRoute']);
app.config(['$routeProvider','$locationProvider','$controllerProvider',function($routeProvider,$locationProvider,$controllerProvider){
  function Addroute(name){
    $routeProvider.when('/'+name,{
      templateUrl:'/resources/templates/'+name+'.html',
      controller:name+'Ctrl',
      resolve:['$q','$rootScope',function($q,$rootScope){
        var defer=$q.defer();
        $.getScript('/resources/js/'+name+'.js',function(data,status){
          if(status=='success'){
            //browserify doesn't allow dynamic loading so doing dirty hack instead
            $controllerProvider.register(name+'Ctrl',['$scope','$q',global[name+'Ctrl']]);
            $rootScope.$apply(function(){
              defer.resolve();
            });
          }else{
            defer.error();
            console.error('unable to load module');
          }
        });
        return defer.promise;
      }]
    });
  }
  
  Addroute('MessagePack');
  Addroute('openimage');
  Addroute('LimeLight');
  Addroute('glComposite');
  
  $routeProvider.otherwise({
    controller:'workDefCtrl'
  });
  $locationProvider.html5Mode(true);
  
}]);


app.directive('resTriplebar',function(){//Would love to do this in pure svg but firefox will not work with xlink
  return{
    restrict:'A',
    link:function($scope,$element,$attrs){
      var toggle=false;
      var s=snap($element[0]);
      var pathsM={
        top:["M5 25L50 25L95 25","M5 25L50 50L95 25"],
        middle:["M5 50L95 50","M50 50L50 50"],
        bottom:["M5 75L50 75L95 75","M5 75L50 50L95 75"]
      }
      var paths={};
      for(var i in pathsM){
        paths[i]=s.path(pathsM[i][0]);
        paths[i].attr({stroke:'white',strokeWidth:10,fill:'none',strokeLinecap:'round'});
      }
      
      function animate(state){
        for(var i in paths){console.log(pathsM[i][state])
          paths[i].animate({d:pathsM[i][state]},250);
        }
      }
      var menu=$('#'+$attrs.resTriplebar);
      menu.on('click',click)
      $element.on('click',click);
      function click(){
        if(toggle){
          menu.removeClass('show');
          animate(0);
        }else{
          menu.addClass('show');
          animate(1);
        }
        toggle=!toggle;
      }
    }
  }
});


app.directive('resLoad',function(){
  return{
    scope:{
      resLoad:'='
    },
    link:function($scope,$element,$attrs){
      function load(event){
        var reader=new FileReader();
        reader.onload=function(o){
          $scope.$apply(function(){
            $scope.resLoad=o.target.result;
          });
        }
        reader.readAsArrayBuffer(event.target.files[0])
      }
      $element.on('change',load);
      $scope.$on('$destroy',function(){
        $element.off('change',load);
      });          
    }
  };
});


app.directive('resDraw',function(){
  return{
    link:function($scope,$element,$attrs){
      $scope.$watch($attrs.resDraw,function(newval,oldval){
        $element.empty();console.log($scope);
        $element.append(newval);
      });
    }
  };
});


app.directive('resDirect',function(){
  return{
    link:function($scope,$element,$attrs){console.log('here');
      $scope[$attrs.resDirect]=$element;
    }
  }
});


app.directive('resNav',function(){
  return{
    link:function($scope,$element,$attrs){
      function test(){
        if($(document).scrollTop()>15){
          $element.addClass('stick');
        }else{
          $element.removeClass('stick');
        }
      }
      $(document).on('scroll',function(){
        test();
      });
      test();
    }
  };
});


app.directive('resJump',function(){
  return{
    link:function($scope,$element,$attrs){
      var target=$('#'+$attrs.resJump);
      var targetTop
      var targetBottom;
      var windowCenter;
      var isIn;
      function calc(){
        windowCenter=$(window).height()*0.25;
        targetTop=target.offset().top;
        targetBottom=targetTop+target.outerHeight(true);
      }
      function check(){
        var scroll=$(window).scrollTop()+windowCenter;
        if(scroll<=targetBottom&&scroll>=targetTop){
          if(isIn!==true){
            $element.addClass('highlight');
            isIn=true;
          }
        }else{
          if(isIn!==false){
            $element.removeClass('highlight');
            isIn=false;
          }
        }
      }
      
      $(window).on('resize',calc);
      $(window).on('scroll',check);
      $scope.$on('$destroy',function(){
        $(window).off('resize',calc);
        $(window).off('scroll',check);
      });
      window.requestAnimationFrame(function(){
        /* Angularjs is designed to bootstrap with ready state, 
           but webkit will recalculate style and layout after ready state. 
        */
        calc();
        check();
      });

      
      $element.on('click',function(){
        $.smoothScroll({
          scrollTarget:'#'+$attrs.resJump,
          offset:$('nav').height()*-1
        });
      });
    }
  };
});


app.directive('resSection',function(){
  return{
    link:function($scope,$element,$attrs){
      var nav=$('<div class="section-nav"></div>');//angularjs directives will replace elements contents 
      var sections=$element.find('section');
      var sectionPos=[];
      var circles=[];
      var count=sections.length;
      function compute(){
        nav.empty();
        sectionPos=[];
        circles=[];
        for(var i=0;i<count;i++){
          var section=$(sections[i]);
          var sectionTop=section.offset().top;
          var sectionBottom=sectionTop+section.height();
          sectionPos.push({top:sectionTop,bottom:sectionBottom});
          var circle=$('<div></div>');
          circles.push(circle);
          nav.append(circle);
          
        }
        console.groupCollapsed();
        console.dir(sectionPos);
        console.groupEnd();
      }
      var currentSelected;
      function scrollCheck(){
        var vertScroll=$(window).scrollTop()+$(window).height()/2;
        
        for(var i=0;i<count;i++){
          if(vertScroll>sectionPos[i].top&&vertScroll<sectionPos[i].bottom){
            if(currentSelected!==i){
              nav.children().removeClass('selected');
              circles[i].addClass('selected');
              currentSelected=i;
              break;
            }
          }
        }
      }
      
      function resizeCheck(){
        currentSelected=undefined;
        compute();
        scrollCheck();
      }
      
      $(window).on('resize',resizeCheck);
      $(window).on('scroll',scrollCheck);
      $scope.$on('$destory',function(){
        $(window).off('resize',resizeCheck);
        $(window).off('scroll',scrollCheck);
      });
      
      window.requestAnimationFrame(resizeCheck);
      $element.append(nav);
      
    }
  };
});


app.directive('resAni',function(){
  return{
    link:function($scope,$element,$attrs){
      var s=snap($element[0]);
      var ani=$scope[$attrs.resAni](s);
      
      var finished=false;
      
      function scrollCheck(){
        var vertScroll=$(window).scrollTop()+$(window).height()/2;
        var elVertCenter=$element.height()/2+$element.offset().top;

        if(vertScroll>=(elVertCenter-50)&&vertScroll<=(elVertCenter+50)&&finished===false){
           ani();
           finished=true;
        }
      }
      if(ani){
        $(window).on('scroll',scrollCheck);
        scrollCheck();
      }
      $scope.$on('$destroy',function(){
        $(window).off('scroll',scrollCheck);
      });
    }
  };
});


app.directive('resDisperse',function(){
  return{
    link:function($scope,$element,$attrs){
      var state='pushedIn';
      var targets=[];
      var centerX;
      function calc(){
        targets=[];
        centerX=$(window).width()/2;
        var elements=$element.find('.disperse-target');
        for(var i=0;i<elements.length;i++){
          var el=$(elements[i]);
          var elCenter=el.width()/2+el.offset().left;
          var direction;
          var distance=0;
          if(elCenter<centerX){
            direction='left';
          }else if(elCenter>centerX){
            direction='right';
          }else{
            direction=Math.random()<=0.5?'left':'right';
          }
          var distance;
          if(direction=='left')
            distance=elCenter;
          else
            distance=$(window).width()+el.width();
          
          targets.push({direction:direction,el:el,distance:distance});
        }
        targets=targets.sort(function(a,b){return a.distance-b.distance});
      }
      $(window).on('resize',calc);
      $scope.$on('$destroy',function(){
        $(window).off('resize',calc);
      });
      
      window.requestAnimationFrame(calc);
      
      $scope.$watch($attrs.resDisperse,function(newValue,oldValue){ 
        calc()
        if(newValue===true&&state==='pushedIn'){
          for(var i=0;i<targets.length;i++){
            var col=Math.floor(targets[i].distance/targets[i].el.width());
            targets[i].el.css('transition-delay',Math.abs(col*100+Math.floor(Math.pow(Math.random(),0.5)*50))+'ms');
            targets[i].el.addClass('disperse-'+targets[i].direction);
          }
          state='pushedOut';
        }else if(newValue===false&&state==='pushedOut'){
          for(var i=0;i<targets.length;i++){
            var col=Math.floor((centerX-targets[i].distance)/targets[i].el.width());
            targets[i].el.css('transition-delay',Math.abs(col*100+Math.floor(Math.pow(Math.random(),1)*50)+'ms'));
            targets[i].el.removeClass('disperse-'+targets[i].direction);
          }
          state='pushedIn';
        }
      });
    }
  }; 
});


app.directive('resScratch',function(){
  return{
    link:function($scope,$element,$attrs){
  /*    var section=$element.children('section');
      var height=$element.height();
      var width=$element.width();
      var pvert=(section.outerHeight(true)-section.outerHeight())/2;
      var phort=(section.outerWidth(true)-section.outerWidth())/2;
      
      var canvas=$('<canvas></canvas>');
      canvas.height(height);
      canvas.width(width);
      canvas[0].height=height*2;
      canvas[0].width=width*2;
      $element.append(canvas);
      
      var p=new paper.PaperScope();
      p.setup(canvas[0]);
      //p.view.center=new p.Point(width,height);
      function line(x1,y1,x2,y2,point){
        var t=new p.Path({
          segments:[[x1,y1],[x2,y2]],
          strokeWidth:1,
          strokeColor:{
            gradient:{
              stops:['rgba(96,96,96,1)',['rgba(96,96,96,1)',point],'rgba(96,96,96,0.2)']
            },
            origin:[x1,y1],
            destination:[x2,y2]
          }
        });
        t.scale(0.95+0.05*Math.random());
        return t;
      }
      line(0,pvert,width,pvert,0.8);
      line(0,height-pvert,width,height-pvert,0.8);
      line(pvert,0,pvert,height,0.8);
      line(width-pvert,0,width-pvert,height,0.8);
      
      var h2=$element.find('h2');
      var h2l=h2.position().left+phort;
      var h2t=h2.position().top+pvert;
      
      var para=$element.find('p');
      var paral=para.position().left+phort+(para.outerWidth(true)-para.outerWidth());
      var parat=para.position().top+pvert;
      
      line(h2l,h2t-25,h2l,h2t+150,0.8);
      line(h2l-20,h2t+h2.height(),h2l+h2.width(),h2t+h2.height(),0.8);
      
      line(paral,parat-15,paral,parat+para.height()+25,0.8);
      line(paral-35,parat,paral+para.width()+25,parat,0.8);
      
      p.view.draw();*/
    }
  };
});


app.controller('workDefCtrl',function(){});//dummy controller just for location checking


app.controller('resumeCtrl',['$scope','$timeout',function($scope,$timeout){
  var duration=500;
  $scope.$on('$routeChangeSuccess',function(event,route){
    if(route.controller=='workDefCtrl'){
      $scope.showDemo=false;
      $timeout(function(){
        $scope.dispFire=false;
      },250);
    }else{
      $.smoothScroll({
        scrollTarget:'#projects',
        offset:$('nav').height()*-1
      });
      $scope.dispFire=true;
      $timeout(function(){
        $scope.showDemo=true;
        
      },750);
    }
  });
  $scope.dispFire=false;
  $scope.evDisperse=function(){
    if($scope.dispFire==false){
      $scope.dispFire=true;
      $timeout(function(){
        $scope.showDemo=true;
        
      },750);
    }else{
      $scope.showDemo=false;
      $timeout(function(){
        $scope.dispFire=false;
      },250);
    }
  };
  $scope.query=function(s){
    var t=[
      s.text(0,25).attr({fill:'white',fontSize:'12px'}),
      s.text(0,50).attr({fill:'white',fontSize:'12px'}),
      s.text(0,75).attr({fill:'white',fontSize:'12px'}),
      s.text(0,100).attr({fill:'white',fontSize:'12px'}),
      s.text(0,125).attr({fill:'white',fontSize:'12px'})
    ];
    var str='SELECT encode(m.Id,\'base64\') as Id, usr AS "User", (SELECT array_agg(tag) FROM (SELECT t.Id,t.Name FROM "Tag" t WHERE mtl.MediaId = m.Id) tag("Id","Name")) AS "Tags", (SELECT array_agg(tag) FROM (SELECT p.Id,p.Name FROM Pool p) AS Pools FROM Media m WHERE m.Id = decode($1::string,\'base64\');'.split(/(?!$)/);
    return function(){
      snap.animate(0,1000,function(step){
        var seg=[];
        var j=0;
        for(var i=0;i<Math.floor(str.length*(step/1000));i++){
          seg.push(str[i]);

          if(seg.length>60){
            if(j>=t.length)
              break;
            t[j].attr({text:seg.join('')});
            seg=[];
            j++;
          }
        }
        if(j<str.length)
          t[j].attr({text:seg.join('')});
      },duration);
    }
  };
  $scope.cycle=function(s){
    var cycleLineM=["m 75.78099,29.666857 a 72.961443,72.444013 0 0 1 63.47769,9.208587 72.961443,72.444013 0 0 1 31.61657,55.423687",
                    "m 168.74867,113.86903 a 72.444013,72.961443 30.09826 0 1 -39.80004,50.30081 72.444013,72.961443 30.09826 0 1 -63.80593,-0.44051",
                    "M 49.32334,152.11687 A 72.961443,72.444013 60.147804 0 1 25.71313,92.478193 72.961443,72.444013 60.147804 0 1 58.04519,37.468813"];
    var cycleLineP=[];
    var cycleLineL=[];
    var cycleArrowM=["m 183.15746,83.618541 -12.66764,15.03682 -14.41315,-12.51732",
                     "m 68.22384,179.71168 -6.65672,-18.50039 18.05753,-6.19254",
                     "m 42.66813,32.13272 19.34712,3.502015 -3.68207,18.731376"];
    var cycleArrowP=[];
    for(var i=0;i<cycleLineM.length;i++){
      var path=s.path();
      cycleLineP.push(path);
      cycleLineL.push(snap.path.getTotalLength(cycleLineM[i]));
      var arrow=s.path(cycleArrowM[i]);
      arrow.attr({opacity:0});
      cycleArrowP.push(arrow);
    }
    return function(){
      snap.animate(0,1000,function(step){
        for(var i=0;i<cycleLineM.length;i++){
          if(step<=900)
            cycleLineP[i].attr({path:snap.path.getSubpath(cycleLineM[i],0,cycleLineL[i]*(step/900))});
          else
            cycleLineP[i].attr({path:cycleLineM[i]});
          if(step>=900){
            cycleArrowP[i].attr({opacity:(step-900)/100});
          }
        }
        
      },duration);
    };
  };
  $scope.flow=function(s){
    var l=s.line(75,100,75,100);
    var c1=s.circle(75,100,0);
    
    var t1=s.text(0, 108, "CSS");

    var c2=s.circle(375,100,0);
    
    var m1="m 275,50 c 40,00 35,50 75,50";
    var m1l=snap.path.getTotalLength(m1);
    var p1=s.path();
    var c3=s.circle(275,50,0);
    c3.attr({fill:'white'});
    var t2=s.text(95, 58, "JAVASCRIPT");
    var m2="m 200,150 c 40,00 35,-50 75,-50";
    var m2l=snap.path.getTotalLength(m2);
    var p2=s.path();
    var c4=s.circle(200,150,0);
    c4.attr({fill:'white'});
    var t3=s.text(90, 158, "HTML5");
    
    return function(){
      var length=Snap.path.getTotalLength(l);
      snap.animate(0,1000,function(step){
        if(step<=950) l.attr({x2:75+300*(step/950)});
        if(step>10&&step<=60) c1.attr({r:7*((step-10)/50)});
        if(step>60) c1.attr({r:7});
        if(step>666&&step<=716) c3.attr({r:7*((step-666)/50)});
        if(step>716) c3.attr({r:7});
        
        if(step>666&&step<=830) p1.attr({path:snap.path.getSubpath(m1, 0,m1l*((step-666)/164))});
        if(step>830) p1.attr({path:m1});
        if(step>416&&step<=466) c4.attr({r:7*((step-416)/50)});
        if(step>466) c4.attr({r:7});
        if(step>416&&step<=580) p2.attr({path:snap.path.getSubpath(m2, 0,m2l*((step-416)/164))});
        if(step>580) p2.attr({path:m2});
        if(step>950){
          l.attr({x2:375});
          c2.attr({r:7*((step-950)/50)});
        }
      },duration);
    };
  };
  $scope.cloud=function(s){
    var outline=s.path("m 100,125 -53.57143,0 c 0,0 -26.84084,0.008 -28.39286,-21.42858 -1.55201,-21.43645 15.35715,-25.89285 15.35715,-25.89285 0,0 -6.78571,-18.92857 7.85714,-31.60714 14.64285,-12.67857 28.39286,-5 28.39286,-5 0,0 8.21428,-29.82143 40.35714,-28.75 32.14286,1.07143 36.60714,33.92857 36.60714,33.92857 0,0 40.53572,-0.17857 41.78572,37.85714 1.25,38.03571 -38.21429,41.07143 -38.21429,41.07143 z");
    var bolt1=s.path("m 40.17857,125 -10.29214,23.96447 -6.49357,-6.2859 -12.59781,54.90956 24.94987,-40.0089 6.70649,6.5638 20.77031,-37.51421");
    bolt1.attr({
      strokeLinejoin:"miter",
      strokeOpacity:0
    });
    var bolt2=s.path("m 150,125 8.86357,17.17877 5.42214,-4.50019 15.09781,45.98105 -28.52129,-32.50894 -4.56364,3.7067 -22.91316,-30.37143");
    bolt2.attr({//144.10714,968.61216
      strokeLinejoin:"miter",
      strokeOpacity:0
    });
    return function(){
      snap.animate(0,900,function(step){
        if(step>0&&step<96)
          bolt1.attr({strokeOpacity:1});
        if(step>96&&step<192)
          bolt1.attr({strokeOpacity:0});
        if(step>192&&step<288)
          bolt1.attr({strokeOpacity:1});
        if(step>288)
          bolt1.attr({strokeOpacity:0});
        
        if(step>500&&step<596)
          bolt2.attr({strokeOpacity:1});
        if(step>596&&step<792)
          bolt2.attr({strokeOpacity:0});
        if(step>792&&step<888)
          bolt2.attr({strokeOpacity:1});
        if(step>888)
          bolt2.attr({strokeOpacity:0});
      },duration)
    };
    
  };
  $scope.tag=function(s){
    var lefttag=s.path("M 100,50 100,100, 100,150");
    lefttag.attr({strokeLinejoin:"miter"});
    var righttag=s.path("M 100,50 100,100, 100,150");
    lefttag.attr({strokeLinejoin:"miter"});
    var slash=s.path("M 100,50 100,150");
    
    return function(){
      lefttag.animate({d:"M 50,50 25,100, 50,150"},duration,mina.easeout);
      righttag.animate({d:"M 150,50 175,100, 150,150"},duration,mina.easeout);
      slash.animate({d:"M 125,50 100,150"},duration,mina.easeout);
    }
  }
  $scope.db=function(s){
    var cylinderTop=s.path("m 35.457807,39.64 0,20.79804 c 1.985903,7.86557 32.329885,13.89032 67.827443,13.46702 35.49756,-0.4233 62.74893,-7.13485 60.91458,-15.00223 l 0,-19.26283 m 0,-0.38006 a 64.464287,14.285714 0 0 1 -60.91458,15.00224 64.464287,14.285714 0 0 1 -67.827443,-13.46703 64.464287,14.285714 0 0 1 60.625041,-15.05972 64.464287,14.285714 0 0 1 68.086242,13.40274");
    var maskPath=s.path("m 35.457807,28.79 0,31.64903 c 1.985903,7.86557 32.329885,13.89032 67.827443,13.46702 35.49756,-0.4233 62.74893,-7.13485 60.91458,-15.00223 l -0.0154,-30.94232 c -2.15275,-8.11454 -32.61252,-14.11052 -68.101594,-13.65365 -35.489074,0.45687 -62.610917,7.19415 -60.625029,14.48215 z");
    var maskRect=s.rect(0,0,200,200);
    maskRect.attr({style:"fill:#fff"});
    maskPath.attr({style:"fill:#000"});
    var mask=s.g(maskRect,maskPath);
    var cylinderMid=cylinderTop.clone();
    
    var maskMid=mask.clone();
    maskMid.attr({
      transform:'translate(0,0)'
    });
    var cylinderBottom=cylinderTop.clone();
    var mid=s.g(cylinderMid);
    mid.attr({
      mask:mask
    });
    cylinderMid.attr({
      transform:'translate(0,0)'
    });
    var bottom=s.g(cylinderBottom);
    bottom.attr({
      mask:maskMid
    });
    cylinderBottom.attr({
      transform:'translate(0,0)'
    });
    return function(){
      cylinderMid.animate({transform:"translate(0,35)"},250,mina.easeout,function(){
        maskMid.attr({transform:"translate(0,35)"});
        cylinderBottom.attr({transform:"translate(0,35)"});
        cylinderBottom.animate({transform:"translate(0,70)"},250,mina.easeout);
      });
    }
  }
}]);