var Engine=require('./glComposite/engine.js');

var mod=[
  "precision mediump float;",
  "precision mediump sampler2D;",
  "varying vec2 vTexCoord;",
  "uniform sampler2D uImage;",
  "uniform float t;",
  "void main(){",
  "  vec4 color = texture2D(uImage,vTexCoord);",
  "  gl_FragColor = vec4(t,0.5,color.b,1.0);",
  "}"
].join("\n");
global.glCompositeCtrl=function glCompositeCtrl($scope,$q){
  var video;
  var canvas;
  var color=0;
  var videoDefer=$q.defer();
  var canvasDefer=$q.defer();
  var action='Hue';
  var radius=0;
  $scope.selected='Hue';
  
  $scope.color='0,50';
  
  $scope.$watch('video',function(newval){
    if(newval!==undefined && newval[0] instanceof HTMLMediaElement){
      video=newval;
      videoDefer.resolve();
    }
  });
  $scope.$watch('canvas',function(newval){
    if(newval!==undefined && newval[0] instanceof HTMLCanvasElement){
      canvas=newval;
      canvasDefer.resolve();
    }
  });
  $scope.state=function(val){
    action=val;
    $scope.selected=val;
  }
  $scope.$watch('color',function(newval){
    if(newval!==undefined){
      color=newval.split(',')[0]/360;
    }
  });
  $scope.$watch('blur',function(newval){
    if(newval!==undefined)
      radius=newval;
  });
  $q.all([videoDefer.promise,canvasDefer.promise]).then(function(){
    var compositor=new Engine(canvas[0].width,canvas[0].height,canvas[0]);
    compositor.AddProgram('mod','noop',mod);
    var count=0.0;
    function animate(){
      compositor.RootImage(video[0]);
      if(action=='Hue'){
        compositor.Render('RGBtoHSL');
        compositor.Render('mod',{t:color});//(count+=0.005)%1.0
        compositor.Render('HSLtoRGB');
      }
      if(action=='Blur'){//TODO add video blur as example.
  //      compositor.Render('Blur',{bRadius:radius});
      }
      compositor.Render('FlipY',null,true);//2d canvas uses top left as origin, webgl uses bottom left as origin, have to vertically flip to fix this.
    }
    video[0].addEventListener('play',function(){
      window.setInterval(animate,16);
    });
  });
}