module.exports=function Texture(canvas,width,height,useFloat){
  var gl=canvas.getContext('webgl',{ preserveDrawingBuffer: true });
  if(gl===null)
    gl=canvas.getContext('experimental-webgl',{ preserveDrawingBuffer: true });
  var texture;
  var fbo;
  var currentImage=false;
  var cache=false;
  var useFramebuffer=false;
  canvas.addEventListener("webglcontextlost",function(event){
    cache=false;
  },false);
  
  function compile(){
    texture=gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D,texture);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,gl.CLAMP_TO_EDGE);
    var type=useFloat?gl.FLOAT:gl.UNSIGNED_BYTE;
    gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,width,height,0,gl.RGBA,type,null);
    if(useFramebuffer){
      fbo=gl.createFramebuffer();
      gl.bindFramebuffer(gl.FRAMEBUFFER,fbo);
      gl.framebufferTexture2D(gl.FRAMEBUFFER,gl.COLOR_ATTACHMENT0,gl.TEXTURE_2D,texture,0);
      if(gl.checkFramebufferStatus(gl.FRAMEBUFFER)!=gl.FRAMEBUFFER_COMPLETE){
        throw new Error("Composite.Inits: Unable to create framebuffer");
      }
      gl.bindFramebuffer(gl.FRAMEBUFFER,null);
    }
    gl.bindTexture(gl.TEXTURE_2D,null);
    cache=true;
  }
  Object.defineProperty(this,'useFramebuffer',{
    set:function(val){
      useFramebuffer=val?true:false;
    },
    get:function(val){
      return useFramebuffer;
    }
  });
  
  this.LoadImage=function LoadImage(image){
    currentImage=image;
    //  texImage2D(long target, int level, long internalformat, int width, int height, int border, long format, long type, ArrayBufferView pixels);
    var type=gl.UNSIGNED_BYTE;
    if(image instanceof Float32Array)
      type=gl.FLOAT;
    gl.bindTexture(gl.TEXTURE_2D,texture);
    if(image instanceof Float32Array||image instanceof Uint8Array)
      gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,width,height,0,gl.RGBA,type,currentImage);
    else
      gl.texSubImage2D(gl.TEXTURE_2D,0,0,0,gl.RGBA,gl.UNSIGNED_BYTE,image);
    gl.bindTexture(gl.TEXTURE_2D,null);
  };
  
  this.BindFramebuffer=function BindFramebuffer(){
    if(!useFramebuffer)
      throw new Error('Compositor.Texture: Texture is not set to become a framebuffer.');
    if(fbo===undefined){//Enable framebuffer support for the lazy.
      fbo=gl.createFramebuffer();
      gl.bindFramebuffer(gl.FRAMEBUFFER,fbo);
      gl.framebufferTexture2D(gl.FRAMEBUFFER,gl.COLOR_ATTACHMENT0,gl.TEXTURE_2D,texture,0);
      if(gl.checkFramebufferStatus(gl.FRAMEBUFFER)!=gl.FRAMEBUFFER_COMPLETE){
        throw new Error("Composite.Inits: Unable to create framebuffer");
      }
      useFramebuffer=true;
    }else
      gl.bindFramebuffer(gl.FRAMEBUFFER,fbo);
  }
  
  this.ApplyTexture=function ApplyTexture(location,index){
    if(cache==false){
      compile();
      if(currentImage!==false)
        this.LoadImage(currentImage);
      cache=true;
    }
    gl.activeTexture(gl['TEXTURE'+index]);
    gl.bindTexture(gl.TEXTURE_2D,texture);
    gl.uniform1i(location,index);
  }
  compile();
};