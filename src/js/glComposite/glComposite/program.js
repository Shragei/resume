module.exports=function ProgramCompiler(canvas,vertexType,fragment){
  var gl=canvas.getContext('webgl',{ preserveDrawingBuffer: true });
  if(gl===null)
    gl=canvas.getContext('experimental-webgl',{ preserveDrawingBuffer: true });
  var VertexSet=require('./vertexSet.js');
  var vertexShader;
  var fragmentShader;
  var program;
  var uniforms={};
  
  var cache=false;
  
  if(!(vertexType in VertexSet))
    throw new Error('Composite.ShaderCompiler: Unsupported vertex type.');
  
  
  canvas.addEventListener("webglcontextlost",function(event){
    cache=false;
  },false);
  
  function CheckShader(gl,shader){
    if(!gl.getShaderParameter(shader,gl.COMPILE_STATUS)){
      console.error("Composite.ShaderCompiler: An error occurred compiling the shaders: "+gl.getShaderInfoLog(shader));
    }
  }
  /* Compile the shader from source to machine code */
  function CompileShader(code,type){
    var shader=gl.createShader(type);
    gl.shaderSource(shader,code);
    gl.compileShader(shader);
  
    CheckShader(gl,shader);
    
    return shader;
  }
  var compile=function(){
    this.HasLayer=false;
    this.HasImage=false;
    
    vertexShader=CompileShader(VertexSet[vertexType],gl.VERTEX_SHADER);
    fragmentShader=CompileShader(fragment,gl.FRAGMENT_SHADER);

    program=gl.createProgram();
    gl.attachShader(program,vertexShader);
    gl.attachShader(program,fragmentShader);
    gl.linkProgram(program);

    var uniformCount=gl.getProgramParameter(program,gl.ACTIVE_UNIFORMS);
    for(var i=0;i<uniformCount;i++){
      var uniform=gl.getActiveUniform(program,i);
      if(uniform.name=='uResolution')
        continue;
      if(uniform.name=='uLayer'){
        this.HasLayer=true;
        continue;
      }
      if(uniform.name=='uImage'){
        this.HasImage=true;
        continue;
      }
      
      uniforms[uniform.name]={
        Type:uniform.type,
        Location:gl.getUniformLocation(program,uniform.name),
        Index:i
      }
    }
  }.bind(this);
  
  this.HasLayer=false;
  this.HasImage=false;
  
  this.UseProgram=function UseProgram(){
    if(cache==false){
      compile();
      cache=true;
    }
    gl.useProgram(program);
  };
  this.GetProgram=function GetProgram(){
    return program;
  };
  /*
  0x8B50:'FLOAT_VEC2',
  0x8B51:'FLOAT_VEC3',
  0x8B52:'FLOAT_VEC4',
  0x8B53:'INT_VEC2',
  0x8B54:'INT_VEC3',
  0x8B55:'INT_VEC4',
  0x8B56:'BOOL',
  0x8B57:'BOOL_VEC2',
  0x8B58:'BOOL_VEC3',
  0x8B59:'BOOL_VEC4',
  0x1400: 'BYTE',
  0x1401: 'UNSIGNED_BYTE',
  0x1402: 'SHORT',
  0x1403: 'UNSIGNED_SHORT',
  0x1404: 'INT',
  0x1405: 'UNSIGNED_INT',
  0x1406: 'FLOAT'
  */
  
  this.GetUniforms=function GetUniforms(){
    return Object.keys(Uniforms);
  }
  
  this.SetUniform=function SetUniforms(name,value){
    function checkArray(value,size){
      if(!(value instanceof Array&&value.length==size))
            throw new Error('Composite.Compute: requires parameter '+name+' be a array of '+size+' values'); 
    }
    if(!(name in uniforms))
      throw new Error('Composite.Compiler.SetUniform: Uniform '+name+" doesn't exist in shader");
 
    switch(uniforms[name].Type){
      case 0x8B50: //FLOAT_VEC2
        checkArray(value,2);
        gl.uniform2f(uniforms[name].Location,value[0],value[1]);
        break;
      case 0x8B51: //FLOAT_VEC3
        checkArray(value,3);
        gl.uniform3f(uniforms[name].Location,value[0],value[1],value[2]);
        break;
      case 0x8B52: //FLOAT_VEC4
        checkArray(value,4);
        gl.uniform4f(uniforms[name].Location,value[0],value[1],value[2],value[3]);
        break;
      case 0x8B53: //INT_VEC2
        checkArray(value,2);
        gl.uniform3i(uniforms[name].Location,value[0],value[1]);
        break;
      case 0x8B54: //INT_VEC3
        checkArray(value,3);
        gl.uniform3i(uniforms[name].Location,value[0],value[1],value[2]);
        break;
      case 0x8B55: //INT_VEC4
        checkArray(value,4);
        gl.uniform4i(uniforms[name].Location,value[0],value[1],value[2],value[3]);
        break;
      case 0x1404: //INT
        gl.uniform1i(uniforms[name].Location,value);
        break;
      case 0x1406: //FLOAT
        gl.uniform1f(uniforms[name].Location,value);
        break;
    }
  };
  
  this.UniformLocation=function UniformLocation(name){
    return gl.getUniformLocation(program,name);
  }
};