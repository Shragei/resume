function Composite(width,height){
  var canvas=document.getElementById('c');document.createElement('canvas');
  canvas.height=height;
  canvas.width=width;
  var gl=canvas.getContext('webgl');
  gl.height=height;
  gl.width=width;
  var VertexSet={
    'noop':[
      "attribute vec2 aPosition;",
      "attribute vec2 aTexCoord;",
      "uniform vec2 uResolution;",
      "varying vec2 vTexCoord;",
      "void main() {",
      "  vec2 zeroToOne = aPosition / uResolution;",
      "   vec2 zeroToTwo = zeroToOne * 2.0;",
      "   vec2 clipSpace = zeroToTwo - 1.0;",
      "   gl_Position = vec4(clipSpace * vec2(1, 1), 0, 1);",
      "   vTexCoord = aTexCoord;",
      "}"
    ].join("\n"),
    'flipY':[
      "attribute vec2 aPosition;",
      "attribute vec2 aTexCoord;",
      "uniform vec2 uResolution;",
      "varying vec2 vTexCoord;",
      "void main() {",
      "  vec2 zeroToOne = aPosition / uResolution;",
      "   vec2 zeroToTwo = zeroToOne * 2.0;",
      "   vec2 clipSpace = zeroToTwo - 1.0;",
      "   gl_Position = vec4(clipSpace * vec2(1, -1), 0, 1);",
      "   vTexCoord = aTexCoord;",
      "}"
    ].join("\n"),
    'flipX':[
      "attribute vec2 aPosition;",
      "attribute vec2 aTexCoord;",
      "uniform vec2 uResolution;",
      "varying vec2 vTexCoord;",
      "void main() {",
      "  vec2 zeroToOne = aPosition / uResolution;",
      "   vec2 zeroToTwo = zeroToOne * 2.0;",
      "   vec2 clipSpace = zeroToTwo - 1.0;",
      "   gl_Position = vec4(clipSpace * vec2(-1, 1), 0, 1);",
      "   vTexCoord = aTexCoord;",
      "}"
    ].join("\n"),
    'flipXY':[
      "attribute vec2 aPosition;",
      "attribute vec2 aTexCoord;",
      "uniform vec2 uResolution;",
      "varying vec2 vTexCoord;",
      "void main() {",
      "  vec2 zeroToOne = aPosition / uResolution;",
      "   vec2 zeroToTwo = zeroToOne * 2.0;",
      "   vec2 clipSpace = zeroToTwo - 1.0;",
      "   gl_Position = vec4(clipSpace * vec2(-1, -1), 0, 1);",
      "   vTexCoord = aTexCoord;",
      "}"
    ].join("\n")
  };
  
  if(!gl.getExtension('OES_texture_float'))
    throw new Error("Can't use floating point");
  
  var Vars={};
  var Shaders={};
  
  var TexBuff=[//Ping-Pong buffers to keep the compute step from clobbering texture memory.
    CreateRenderBuffer(width,height),
    CreateRenderBuffer(width,height)];//Ping-pong frame and texture buffers for the pipeline
  var CurTexBuff=0;//Index of the current TexBuff
  
  var AuxTextures={
    uInsert:ImageToTexture(),
    hasuInsert:false,
    uMask:ImageToTexture(),
    hasuMask:false
  };
  
  InitVars();
  /* used add a fragment shader to list of usable operations */
  this.AddFragment=function AddFragment(shaderName,fragment,vertex){
    var program=CompileProgram(fragment,vertex);
    Shaders[shaderName]={
      Program:program,
      Uniforms:{}
    };
    
    //0x8B5E - image
    
    var uniformCount=gl.getProgramParameter(program,gl.ACTIVE_UNIFORMS);
    for(var i=0;i<uniformCount;i++){
      var uniform=gl.getActiveUniform(program,i);
      Shaders[shaderName].Uniforms[uniform.name]={
        Type:uniform.type,
        Location:gl.getUniformLocation(program,uniform.name),
        Index:i
      };
    }
  /*  function addUniform(uniformName){
      var Index=gl.getUniformLocation(program,uniformName);
      if(Index!==null)//if the uniform doesn't exist don't add it
        Shaders[shaderName].Uniforms[uniformName]=Index;
    }*/
  };
  /* sets the base image for the processing pipeline */
  this.SetImage=function SetImage(image){
    SetImageToTexture(TexBuff[CurTexBuff].Texture,image);
  };
  /* If the fragment shader supports it, this image will be used as the other half of the operation */
  this.AuxImage=function AuxImage(image){
    SetImageToTexture(AuxTextures.uInsert,image);
    AuxTextures.hasuInsert=true;
  };
  /* If the fragment shader supports it, this image will be used as a mask for the operation */
  this.MaskImage=function MaskImage(image){
    SetImageToTexture(AuxTextures.uMask,image);
    AuxTextures.hasuMask=true;
  };
  /* Run a single fragment shader operation on the pipeline */
  this.Compute=function(name,parameters){
    Compute(name,parameters,true);
  }
  /* Returns the image data for the last operation */
  this.GetResult=function GetResult(type){
    var rawPixels;
    if(type==='byte'){
      type=gl.UNSIGNED_BYTE;
      rawPixels=new Uint8ClampedArray(width*height*4);
    }else if(type==='float'){
      type=gl.FLOAT;
      rawPixels=new Foat32Array(width*height*4);
    }else{
      throw new Error('Composite.GetResult: requres type to be byte or float');
    }
    gl.bindFramebuffer(TexBuff[(CurTexBuff+1)%2].Frame);//Compute incormentor will set CurTexBuff off by one */ 
    gl.readPixels(0,0,width,height,gl.RGBA,type,rawPixels);
    gl.bindFramebuffer(null);
    return rawPixels;
  };
  this.SaveTo=function SaveResult(saveObject){
    Compute('Dummy',null,false);
    if(saveObject instanceof CanvasRenderingContext2D){
      saveCanvasContext(saveObject);
    }else if(saveObject instanceof HTMLCanvasElement){
      var ctx=saveObject.getContext('2d');
      saveCanvasContext(ctx);
    }else if(saveObject instanceof ImageData){
      if(saveObject.height!==height&&saveObject.width!==width){
        throw new Error('Composite.SaveResult: Dimensions of image data must match the initialized dimensions fo Composite.');
      }
      gl.readPixels(0,0,width,height,gl.RGBA,gl.UNSIGNED_BYTE,saveObject.data);
    }else if(saveObject instanceof Uint8ClampedArray){
      
    }else if(saveObject instanceof Float32Array){
      
    }
    function saveCanvasContext(ctx){
      if(ctx.height!==height&&ctx.width!==width){
        throw new Error('Composite.SaveResult: Dimensions of canvas context must match the initialized dimensions fo Composite.');
      }
      ctx.drawImage(canvas,0,0);
    }
    
  }
  
  // It is legal for a WebGL implementation exposing the OES_texture_float extension to
  // support floating-point textures but not as attachments to framebuffer objects.
  
  function Compute(ShaderKey,parameters,frameBuffer){//if return content to the user then turn off the framebuffer for image reading
    if(!(ShaderKey in Shaders))
      throw new Error("Composite.Compute: Shader not found.");
    var program=Shaders[ShaderKey].Program;
    
    var inBuff=frameBuffer?TexBuff[(CurTexBuff++)%2]:TexBuff[(CurTexBuff+1)%2];
    var outBuff=frameBuffer?TexBuff[(CurTexBuff++)%2]:null;
    
    var uniformIndex=0;//used by bindTexture
    var err=gl.getError();
    gl.useProgram(program);
    SetVars(program,width,height);
    
    bindTexture('uImage',inBuff.Texture);
    
    
  /*  setAux('uInsert');
    setAux('uMask');*/
    
    gl.clearColor(0.0, 0.0, 0.0, 0.0);
    gl.clear(gl.COLOR_BUFFER_BIT); 
    
    gl.drawArrays(gl.TRIANGLES,0,6);

    var test=new Uint8Array(4*128*128);
    gl.readPixels(1,1,128,128,gl.RGBA,gl.UNSIGNED_BYTE,test);
    var err=gl.getError();
    
    function setAux(auxName){
      if(AuxTextures['has'+auxName]){
        bindTexture(auxName,AuxTextures[auxName]);
        AuxTextures['has'+auxName]=false;//require insert to be set each time compute is run even if same image is used multiple times as a sanity check
      }else if((auxName in Shaders[ShaderKey].Uniforms)){
        throw new Error('Composite.Computer: Shader requires insert image but none is provided.');
      }
    }
    function bindTexture(name,texture){
      var uniform=Shaders[ShaderKey].Uniforms[name];
      if(uniform!==undefined){
        gl.uniform1i(uniform.Location,uniformIndex);
        gl.activeTexture(gl['TEXTURE'+uniformIndex]);
        gl.bindTexture(gl.TEXTURE_2D,texture);
        uniformIndex++;
      }
    }
  };
  /* Validate that the shader correctly compiled */
  function CheckShader(gl,shader){
    if(!gl.getShaderParameter(shader,gl.COMPILE_STATUS)){
      console.error("Composite: An error occurred compiling the shaders: "+gl.getShaderInfoLog(shader));
    }
  }
  /* Compile the shader from source to machine code */
  function CompileShader(code,type){
    var shader=gl.createShader(type);
    gl.shaderSource(shader,code);
    gl.compileShader(shader);
  
    CheckShader(gl,shader);
    
    return shader;
  }
  function CompileProgram(code,vertex){
    if(!(vertex in VertexSet))
      throw new Error('Composite.CompileShader: Vertex type must be of '+Object.keys(VertexSet).join(', '));
    var vertexShader=CompileShader(VertexSet[vertex],gl.VERTEX_SHADER);
    var fragmentShader=CompileShader(code,gl.FRAGMENT_SHADER);
  
    var program=gl.createProgram();
    gl.attachShader(program,vertexShader);
    gl.attachShader(program,fragmentShader);
    gl.linkProgram(program);
    return program;
  }
  function InitVars(){
    Vars.textureMapBuffer=gl.createBuffer();
    var textureMapVertices=[
      0.0,0.0,1.0,
      0.0,0.0,1.0,
      0.0,1.0,1.0,
      0.0,1.0,1.0];

    gl.bindBuffer(gl.ARRAY_BUFFER,Vars.textureMapBuffer);
    gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(textureMapVertices),gl.STATIC_DRAW);
    
     
    Vars.planeBuffer=gl.createBuffer();
    var planeVertices=[
      0,0,
      width,0,
      0,height,
      0,height,
      width,0,
      width,height];

    gl.bindBuffer(gl.ARRAY_BUFFER,Vars.planeBuffer);
    gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(planeVertices),gl.STATIC_DRAW);
  }
  function SetVars(program,width,height){
    gl.bindBuffer(gl.ARRAY_BUFFER,Vars.textureMapBuffer);
    var aTexCoordIndex=gl.getAttribLocation(program,"aTexCoord");
    gl.enableVertexAttribArray(aTexCoordIndex);
    gl.vertexAttribPointer(aTexCoordIndex,2,gl.FLOAT,false,0,0);
    
    gl.bindBuffer(gl.ARRAY_BUFFER,Vars.planeBuffer);
    var aPositionIndex=gl.getAttribLocation(program,"aPosition");
    gl.enableVertexAttribArray(aPositionIndex);
    gl.vertexAttribPointer(aPositionIndex,2,gl.FLOAT,false,0,0);
    
    var uResolutionIndex=gl.getUniformLocation(program,"uResolution");
    gl.uniform2f(uResolutionIndex,width,height); 
  }
  function ImageToTexture(image){
    var texture=gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D,texture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);

    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);

    if(!(image===undefined||image===null))
      SetImageToTexture(image)
    return texture;
  }

  function SetImageToTexture(texture,image){
    gl.bindTexture(gl.TEXTURE_2D,texture);

    if(image instanceof HTMLImageElement){
      gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,image);
    }else if(image instanceof Uint8Array){
      gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,width,height,0,gl.RGBA,gl.UNSIGNED_BYTE,image);
    }else if(image instanceof Float32Array)
      gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,width,height,0,gl.RGBA,gl.FLOAT,image);
    
   //  texImage2D(long target, int level, long internalformat, int width, int height, int border, long format, long type, ArrayBufferView pixels);
    //gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,width,height,0,gl.RGBA,type,image);
    
    gl.bindTexture(gl.TEXTURE_2D,null);
  }

  function CreateRenderBuffer(width,height){
    var textureBuffer=gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D,textureBuffer);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);

    gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,width,height,0,gl.RGBA,gl.FLOAT,null);
    
    var frameBuffer=gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER,frameBuffer);
    gl.framebufferTexture2D(gl.FRAMEBUFFER,gl.COLOR_ATTACHMENT0,gl.TEXTURE_2D,textureBuffer,0);
        
    gl.bindTexture(gl.TEXTURE_2D, null);
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    
    return {Texture:textureBuffer,Frame:frameBuffer};
  }
  
  var enums = {
  0x8B50:'FLOAT_VEC2',
  0x8B51:'FLOAT_VEC3',
  0x8B52:'FLOAT_VEC4',
  0x8B53:'INT_VEC2',
  0x8B54:'INT_VEC3',
  0x8B55:'INT_VEC4',
  0x8B56:'BOOL',
  0x8B57:'BOOL_VEC2',
  0x8B58:'BOOL_VEC3',
  0x8B59:'BOOL_VEC4',
  0x1400: 'BYTE',
  0x1401: 'UNSIGNED_BYTE',
  0x1402: 'SHORT',
  0x1403: 'UNSIGNED_SHORT',
  0x1404: 'INT',
  0x1405: 'UNSIGNED_INT',
  0x1406: 'FLOAT'
};
  function blah(shader,params){
    var keys=Object.keys(shader.Uniforms);
    for(var i=0;i<keys.length;i++){
      if(keys[i] in ['uImage','uMask','uInsert'])
        continue;
      var uniform=shader.Uniforms[keys[i]];
      var value=params[keys[i]];
      if(value===undefined)
        throw new Error('Composite.Compute: value is undefined for parameter '+keys[i]);
      switch(uniform.Type){
        case 0x8B50:
          if(!(value instanceof Array&&value.length==2))
            throw new Error('Composite.Compute: requires parameter '+keys[i]+' be a array of 2 values');
          gl.uniform3f(uniform.Location,value[0],value[1]);
          break;
        case 0x8B51:
          if(!(value instanceof Array&&value.length==3))
            throw new Error('Composite.Compute: requires parameter '+keys[i]+' be a array of 3 values');
          gl.uniform3f(uniform.Location,value[0],value[1],value[2]);
          break;
        case 0x8B52:
          if(!(value instanceof Array&&value.length==4))
            throw new Error('Composite.Compute: requires parameter '+keys[i]+' be a array of 4 values');
          gl.uniform4f(uniform.Location,value[0],value[1],value[2],value[3]);
          break;
        case 0x1406:
          if(!(value instanceof Number))
            throw new Error('Composite.Compute: requires parameter '+keys[i]+' be a number');
          gl.uniform1f(uniform.Location,value);
      }
    }
  }
}
module.exports=Composite;