module.exports=function Inits(canvas,width,height){
  var gl=canvas.getContext('webgl',{ preserveDrawingBuffer: true });
  if(gl===null)
    gl=canvas.getContext('experimental-webgl',{ preserveDrawingBuffer: true });
  var textureMapVertices=[
    0.0,0.0,1.0,
    0.0,0.0,1.0,
    0.0,1.0,1.0,
    0.0,1.0,1.0];

  var planeVertices=[
    0,0,
    width,0,
    0,height,
    0,height,
    width,0,
    width,height];
  
  var textureMapBuffer;
  var planeBuffer;
  var texture;
  var fbo;
  
  var cache=false;
  
  canvas.addEventListener("webglcontextlost",function(event){
    cache=false;
  },false);
  
  
  function compile(){
    textureMapBuffer=gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER,textureMapBuffer);
    gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(textureMapVertices),gl.STATIC_DRAW);


    planeBuffer=gl.createBuffer();


    gl.bindBuffer(gl.ARRAY_BUFFER,planeBuffer);
    gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(planeVertices),gl.STATIC_DRAW);
/*
    texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,width,height,0,gl.RGBA,gl.FLOAT,null);

    var fbo=gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER,fbo);
    gl.framebufferTexture2D(gl.FRAMEBUFFER,gl.COLOR_ATTACHMENT0,gl.TEXTURE_2D,texture,0);
    gl.bindTexture(gl.TEXTURE_2D,null);
    if(gl.checkFramebufferStatus(gl.FRAMEBUFFER)!=gl.FRAMEBUFFER_COMPLETE){
      throw new Error("Composite.Inits: Unable to create framebuffer");
    }*/
  };
  this.Init=function Init(){
    compile();
  }
  this.Apply=function ApplyVertexParameters(program){
    if(cache==false){
      compile();
      cache=true;
    }
    gl.bindBuffer(gl.ARRAY_BUFFER,textureMapBuffer);
    var aTexCoordIndex=gl.getAttribLocation(program,"aTexCoord");
    gl.enableVertexAttribArray(aTexCoordIndex);
    gl.vertexAttribPointer(aTexCoordIndex,2,gl.FLOAT,false,0,0);
    
    gl.bindBuffer(gl.ARRAY_BUFFER,planeBuffer);
    var aPositionIndex=gl.getAttribLocation(program,"aPosition");
    gl.enableVertexAttribArray(aPositionIndex);
    gl.vertexAttribPointer(aPositionIndex,2,gl.FLOAT,false,0,0);
    
    var uResolutionIndex=gl.getUniformLocation(program,"uResolution");
    gl.uniform2f(uResolutionIndex,width,height); 
  };
}