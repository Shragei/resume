
module.exports=[
  {
    Name:'Test',
    Vertex:'noop',
    Fragment:[
      "precision mediump float;",
      "precision mediump sampler2D;",
      "varying vec2 vTexCoord;",
      "void main(){",
  //   "  gl_FragColor = texture2D(uImage,vTexCoord);",
      "  gl_FragColor = vec4(1,0,1,1.0);",
      "}"].join("\n")
  },
  {
    Name:'FlipY',
    Vertex:'flipY',
    Fragment:[
      "precision mediump float;",
      "varying vec2 vTexCoord;",
      "uniform sampler2D uImage;",
      "void main(){",
      "  gl_FragColor = texture2D(uImage,vTexCoord);",
      "}"].join("\n")
  },
  {
    Name:'Test2',
    Vertex:'noop',
    Fragment:[
      "precision highp float;",
      "precision mediump sampler2D;",
      "varying vec2 vTexCoord;",
      "uniform sampler2D uImage;",
      "void main(){",
      " vec4 color = texture2D(uImage,vTexCoord);",
      "  gl_FragColor =  color.g > 1.0 ? vec4(1,0,1,1) : vec4(0,1,0,1);",
      "}"].join("\n")
  },
  {
    Name:'Solid', 
    Vertex:'noop',
    Fragment:[
      "precision mediump float;",
      "varying vec2 vTexCoord;",
      "uniform vec4 uColor",
      
      "void main(){",
      "  gl_FragColor = uColor;",
      "}"].join("\n")
  },
  {
    Name:'RGBtoHSL', 
    Vertex:'noop',
    Fragment:[// Orginal author of the RGBtoHSV algorithm Chilli Ant http://www.chilliant.com/rgb2hsv.html
      "precision mediump float;",
      "varying vec2 vTexCoord;",
      "uniform sampler2D uImage;",
      "float Epsilon = 1e-10;",
 
      "vec3 RGBtoHCV(vec3 RGB){",
        // Based on work by Sam Hocevar and Emil Persson
      "  vec4 P = (RGB.g < RGB.b) ? vec4(RGB.bg, -1.0, 2.0/3.0) : vec4(RGB.gb, 0.0, -1.0/3.0);",
      "  vec4 Q = (RGB.r < P.x) ? vec4(P.xyw, RGB.r) : vec4(RGB.r, P.yzx);",
      "  float C = Q.x - min(Q.w, Q.y);",
      "  float H = abs((Q.w - Q.y) / (6.0 * C + Epsilon) + Q.z);",
      "  return vec3(H, C, Q.x);",
      "}",

      "vec3 RGBtoHSL(vec3 RGB){",
      "  vec3 HCV = RGBtoHCV(RGB);",
      "  float L = HCV.z - HCV.y * 0.5;",
      "  float S = HCV.y / (1.0 - abs(L * 2.0 - 1.0) + Epsilon);",
      "  return vec3(HCV.x, S, L);",
      "}",
      "void main(){",
      "  vec4 c = texture2D(uImage,vTexCoord);",
      "  gl_FragColor = vec4(RGBtoHSL(c.rgb).xyz,c.w);",
      "}"].join("\n")
  },
  {
    Name:'HSLtoRGB', 
    Vertex:'noop',
    Fragment:[// Orginal author of the RGBtoHSV algorithm Chilli Ant http://www.chilliant.com/rgb2hsv.html
      "precision mediump float;",
      "varying vec2 vTexCoord;",
      "uniform sampler2D uImage;",
      
      "vec3 HUEtoRGB(float H){",
      "  float R = abs(H * 6.0 - 3.0) - 1.0;",
      "  float G = 2.0 - abs(H * 6.0 - 2.0);",
      "  float B = 2.0 - abs(H * 6.0 - 4.0);",
      "  return clamp(vec3(R,G,B),0.0,1.0);",
      "}",
      "vec3 HSLtoRGB(vec3 HSL){",
      "  vec3 RGB = HUEtoRGB(HSL.x);",
      "  float C = (1.0 - abs(2.0 * HSL.z - 1.0)) * HSL.y;",
      "  return (RGB - 0.5) * C + HSL.z;",
      "}",
      "void main(){",
      "  vec4 c = texture2D(uImage,vTexCoord);",
      "  gl_FragColor = vec4(HSLtoRGB(c.rgb).xyz,c.a);",
      "}"].join("\n")
  }
];