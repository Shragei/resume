var Inits=require('./inits');
var Program=require('./program.js');
var Texture=require('./texture.js');
var Presets=require('./presets.js');

var setFragment=[
  "precision mediump float;",
  "varying vec2 vTexCoord;",
  "uniform sampler2D uImage;",
  "uniform sampler2D uLayer;",
  "void main(){",
  "  gl_FragColor = texture2D(uLayer,vTexCoord);",
  "}"
].join("\n");
var noopFragment=[
  "precision mediump float;",
  "varying vec2 vTexCoord;",
  "uniform sampler2D uImage;",
  "void main(){",
  "  gl_FragColor = texture2D(uImage,vTexCoord);",
  "}"
].join("\n");
module.exports=function Engine(width,height,outputCanvas){
  var canvas;
  if(outputCanvas!==undefined){
    canvas=outputCanvas;
  }else{
    canvas=document.createElement('canvas');
    canvas.width=width;
    canvas.height=height;
  }
  var gl=canvas.getContext('webgl',{ preserveDrawingBuffer: true });
  if(gl===null)
    gl=canvas.getContext('experimental-webgl',{ preserveDrawingBuffer: true });
 
  var programs={
    'set':new Program(canvas,'noop',setFragment),
    'noop':new Program(canvas,'noop',noopFragment)
  };
  
  
  if(!gl.getExtension('OES_texture_float'))
    throw new Error("Composite.Engine: WebGL floating-point textures are not supported.")
  
  var constants=new Inits(canvas,width,height);

  var ulayer=new Texture(canvas,width,height,false);

  var framebuffersIdx=0;
  var framebuffers=[new Texture(canvas,width,height,true),
                    new Texture(canvas,width,height,true)];
  for(var i=0;i<2;i++)
    framebuffers[i].useFramebuffer=true;
  
  var context={
    recompute:false,
    defer:null,
    name:null,
    params:null,
    drawto:null
  }
  
  
  this.AddProgram=function AddProgram(name,vertex,fragment){
    programs[name]=new Program(canvas,vertex,fragment);
  };
  
  this.RootImage=function RootImage(image){
    ulayer.LoadImage(image);
    return this.Render('set');
  }
  
  this.Layer=function Layer(image){
    uLayer.LoadImage(image);
  };
  
  this.Render=function Render(name,params,useRoot){
    var error=compute(name,params,null,useRoot);
  };
  this.ListPrograms=function ListPrograms(){
    return Object.keys(programs);
  }
  this.DrawOut=function DrawOut(){
    if(outputCanvas===undefined)
      throw new Error('Composite.DrawOut: An output canvas is require draw to.');
    var error=compute('out',null,drawTo);
  };
  this.SaveTo=function SaveTo(image){
    if(!(image instanceof HTMLCanvasElement || image instanceof CanvasRenderingContext2D))
      throw new Error('Composite.SaveInfo: requires image to be a HTML canvas.');
    var drawTo;
    if(image instanceof HTMLCanvasElement){
      drawTo=image.getContext('2d');
    }else{
      drawTo=image;
    }
    var error=compute('noop',null,drawTo,true);
  }
  function compute(name,params,storeTo,useRoot){
    var framebuffer=framebuffers[framebuffersIdx%2];
    var uImage=framebuffers[(++framebuffersIdx)%2];
    if(useRoot){
      gl.bindFramebuffer(gl.FRAMEBUFFER,null);
    }else
      framebuffer.BindFramebuffer();

    var program=programs[name];
    
    program.UseProgram();
    constants.Apply(program.GetProgram());
    
    if(program.HasImage)
      uImage.ApplyTexture(program.UniformLocation("uImage"),0);
    if(program.HasLayer)
      ulayer.ApplyTexture(program.UniformLocation("uLayer"),1);
    
    if(params instanceof Object){
      var paramNames=Object.keys(params);
      for(var i=0;i<paramNames.length;i++)
        program.SetUniform(paramNames[i],params[paramNames[i]]);
    }
    gl.clearColor(0.0,1.0,0.0,1.0);
    gl.clear(gl.COLOR_BUFFER_BIT); 
    
    gl.drawArrays(gl.TRIANGLES,0,6);
    gl.bindFramebuffer(gl.FRAMEBUFFER,null)
  //  var error=gl.getError();
   // if(error==gl.NO_ERROR){
      if(storeTo){
        if(storeTo instanceof Float32Array){
          gl.readPixels(0,0,width,height,gl.RGBA,gl.FLOAT,storeTo)
        }else{
          var t=new Uint8Array(4*(width)*(height));
          var imageBuff=storeTo.createImageData(width,height);

          gl.readPixels(0,0,width,height,gl.RGBA,gl.UNSIGNED_BYTE,t);
          imageBuff.data.set(t);
  //        storeTo.clearRect(0,0,width,height);
          storeTo.putImageData(imageBuff,0,0);                  
        }
      //  storeTo.drawImage(canvas,0,0);
      }
   // }
    return 0;
  }
  
  for(var i=0;i<Presets.length;i++){
    this.AddProgram(Presets[i].Name,Presets[i].Vertex,Presets[i].Fragment);
  }
};