# README #

### Requirements ###

* Node.js v5.0.0 or greater

### Building ###

* git clone https://bitbucket.org/Shragei/resume.git or download repo's [bundle.](https://bitbucket.org/Shragei/resume/downloads)
* npm install
* npm start