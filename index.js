var express=require('express');
var path=require('path');
var server=express();

server.use('/resources',express.static(path.join(__dirname,'resources')));
function push(req,res){
  res.sendFile(path.join(__dirname,'index.html'));
}
server.get('/',push);
server.get('/MessagePack',push);
server.get('/LimeLight',push);
server.get('/glComposite',push);
server.listen('2046');
